package mx.com.cardif.utils;

import org.apache.commons.text.WordUtils;

import java.util.Calendar;
import java.util.Locale;

public class GetMonth {
    public static String fromIntValue(String monthValue) {
        int mes = Integer.parseInt(monthValue);
        Locale locale = new Locale("es", "ES");
        Calendar calendarInicio = Calendar.getInstance();
        calendarInicio.set(Calendar.MONTH, mes - 1);
        String monthName = calendarInicio.getDisplayName(Calendar.MONTH, Calendar.LONG, locale);
        return WordUtils.capitalize(monthName);
    }

}
