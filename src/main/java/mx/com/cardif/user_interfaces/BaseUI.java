package mx.com.cardif.user_interfaces;

import net.serenitybdd.screenplay.targets.Target;

public class BaseUI {
    public static final Target SELECT_VALUE = Target.the("value: {0}")
            .locatedBy("//li[text()='{0}']");

    public static final Target DATE_PICKER_YEAR = Target.the("date picker month")
            .locatedBy("//span[@class='ui-datepicker-year']");

    public static final Target DATE_PICKER_MONTH = Target.the("date picker month")
            .locatedBy("//span[@class='ui-datepicker-month']");

    public static final Target DATE_PICKER_DAY = Target.the("date picker day: {0}")
            .locatedBy("//a[contains(@class,'ui-state-default') and text()='{0}']");

    public static final Target DATE_PICKER_PREVIOUS_BUTTON = Target.the("date picker previous button")
            .locatedBy("//span[contains(.,'Anterior')]");

    public static final Target CLOSE_CHANGE_PASSWORD = Target.the("close button change password banner")
            .locatedBy("(//span[contains(.,'Cerrar')])[2]");

}
