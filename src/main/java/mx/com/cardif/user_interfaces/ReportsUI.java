package mx.com.cardif.user_interfaces;

import net.serenitybdd.screenplay.targets.Target;

public class ReportsUI {
    public static final Target SALES_REPORT = Target.the("sales report combo box")
            .locatedBy("//label[@id='formReportes:lreportes_label']");

    public static final Target SALE_DATE_FROM = Target.the("sale date from")
            .locatedBy("(//span[@class='ui-button-text'])[1]");

    public static final Target SALE_DATE_TO = Target.the("sale date to")
            .locatedBy("(//span[@class='ui-button-text'])[2]");

    public static final Target PARTNERS = Target.the("partners")
            .locatedBy("//label[@id='formReportes:lsocios_label']");

    public static final Target STATES = Target.the("states")
            .locatedBy("//label[@id='formReportes:lestados_label']");

    public static final Target ZONES = Target.the("zones")
            .locatedBy("//label[@id='formReportes:lZona_label']");

    public static final Target BRANCH_OFFICES = Target.the("branch offices")
            .locatedBy("//label[@id='formReportes:lsucursales_label']");

    public static final Target SALESPERSON = Target.the("salesperson")
            .locatedBy("//label[@id='formReportes:lvendedores_label']");

    public static final Target PRODUCTS = Target.the("products")
            .locatedBy("//label[@id='formReportes:lproductos_label']");

    public static final Target GENERATE_XLS = Target.the("generate xls button")
            .locatedBy("//span[contains(.,'Generar xls')]");

    public static final Target FROM_DATE = Target.the("from date")
            .locatedBy("(//button[contains(@class,'ui-datepicker-trigger')])[1]");

    public static final Target TO_DATE = Target.the("to date")
            .locatedBy("(//button[contains(@class,'ui-datepicker-trigger')])[2]");


}
