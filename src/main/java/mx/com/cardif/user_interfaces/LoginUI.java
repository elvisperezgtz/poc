package mx.com.cardif.user_interfaces;


import net.serenitybdd.screenplay.targets.Target;

public class LoginUI {

    public static final Target USERNAME = Target.the("Username field").locatedBy("#j_idt25");
    public static final Target PASSWORD = Target.the("Password field").locatedBy("#j_idt29");
    public static final Target LOGIN = Target.the("Ingresar button").locatedBy("//span[text()='Ingresar']");

}
