package mx.com.cardif.user_interfaces;

import net.serenitybdd.screenplay.targets.Target;

public class HomeUI {
    public static final Target WELCOME_MESSAGE = Target.the("the welcome message").
            locatedBy("//span[contains(.,'Bienvenid@ {0}')]");
    public static final Target OPTION_MENU = Target.the("{0} options menu").
            locatedBy("//span[@class='ui-menuitem-text'][contains(.,'{0}')]");

    public static final Target PRODUCTS = Target.the("products").
            locatedBy("//a//div");
}
