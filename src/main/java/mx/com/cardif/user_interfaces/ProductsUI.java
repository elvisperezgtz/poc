package mx.com.cardif.user_interfaces;

import net.serenitybdd.screenplay.targets.Target;

public class ProductsUI {

    public static final Target SELECT_PRODUCT = Target.the("select product")
            .locatedBy("//label[contains(.,'Selecciona Un Producto')]");

    public static final Target NEXT = Target.the("next button")
            .locatedBy("//div[@class='clssBtnNext']");

    public static final Target INVOICE_VALUE = Target.the("invoice value text box")
            .locatedBy("//input[contains(@id,'valorFactura')]");

    public static final Target PLAN = Target.the("plan radio button")
            .locatedBy("(//span[@class='ui-radiobutton-icon ui-icon ui-icon-blank ui-c'])[2]");

    public static final Target BRAND = Target.the("brand field")
            .locatedBy("#marca");

    public static final Target VEHICLE_DESCRIPTION = Target.the("vehicle description field")
            .locatedBy("#modelo");

    public static final Target YEAR_MODEL = Target.the("year model field")
            .locatedBy("#anio");

    public static final Target SERIAL_NUMBER = Target.the("serial number field")
            .locatedBy("#numeroSerie");

    public static final Target MOTOR_NUMBER = Target.the("motor number field")
            .locatedBy("#numeroMotor");

    public static final Target INVOICE_DATE = Target.the("invoice date field")
            .locatedBy("#fechaFactura_input");

    public static final Target CONTRACT_NUMBER = Target.the("contract number field")
            .locatedBy("#textContrato");

    public static final Target SALESMAN = Target.the("salesman field")
            .locatedBy("(//span[text()='VENDEDOR']/following::input[@type='text'])[1]");

    public static final Target SALESMAN_EMAIL    = Target.the("salesman email field")
            .locatedBy("(//span[text()='EMAIL VENDEDOR']/following::input[@type='text'])[1]");

    public static final Target INSURANCE_COMPANY = Target.the("insurance company field")
            .locatedBy("(//span[text()='ASEGURADORA PRIMARI']/following::input[@type='text'])[1]");

    public static final Target PLAN_CONTRACT_NUMBER = Target.the("insurance company field")
            .locatedBy("(//span[text()='NO. DE CONTRATO']/following::input[@type='text'])[1]");

}
