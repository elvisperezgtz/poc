package mx.com.cardif.user_interfaces;

import net.serenitybdd.screenplay.targets.Target;

public class ContractorDataUI {
    public static final Target NAME = Target.the("name field")
            .locatedBy("(//td[text()='Nombre']/following::input[@type='text'])[1]");

    public static final Target LASTNAME = Target.the("lastname field")
            .locatedBy("(//td[text()='Apellido Paterno']/following::input[@type='text'])[1]");

    public static final Target BIRTHDAY = Target.the("birthday field")
            .locatedBy("(//td[text()='Fecha de Nacimiento']/following::input[@type='text'])[1]");

    public static final Target RFC = Target.the("RFC field")
            .locatedBy("(//td[text()='RFC']/following::input[@type='text'])[1]");

    public static final Target CONTACT_PHONE_NUMBER = Target.the("contact phone number field")
            .locatedBy("(//td[text()='Teléfono de Contacto']/following::input[@type='text'])[1]");

    public static final Target EMAIL = Target.the("email number field")
            .locatedBy("(//td[text()='Correo Electrónico']/following::input[@type='text'])[1]");

    public static final Target STREET = Target.the("street field")
            .locatedBy("(//td[text()='Calle']/following::input[@type='text'])[1]");

    public static final Target EXTERNAL_NUMBER = Target.the("external number field")
            .locatedBy("(//td[text()='No Exterior']/following::input[@type='text'])[1]");

    public static final Target POSTAL_CODE = Target.the("postal code field")
            .locatedBy("(//td[text()='Código Postal']/following::input[@type='text'])[1]");

    public static final Target STATE = Target.the("state field")
            .locatedBy("//label[@id='frmAsegurado:menuEstados_label']");

    public static final Target DELEGATION = Target.the("delegation field")
            .locatedBy("//label[contains(.,'Delegaciones/Municipios')]");

    public static final Target COLONY = Target.the("colony field")
            .locatedBy("//label[contains(.,'Selecciona Una Colonia')]");

    public static final Target HIDALGO = Target.the("Hidalgo option")
            .locatedBy("//li[text()='Hidalgo']");

    public static final Target GENDER = Target.the("gender ")
            .locatedBy("(//td[@role='gridcell'][contains(.,'Sexo')]/following::div)[3]");

}
