package mx.com.cardif.user_interfaces;

import net.serenitybdd.screenplay.targets.Target;

public class PaymentMethodUI {

    public static final Target PAYMENT_METHOD = Target.the("payment method")
            .locatedBy("//label[contains(.,'Medios de Cobro')]");

    public static final Target NAME = Target.the("name on the card")
            .locatedBy("//input[@id='frmCobros:inputTHab']");

    public static final Target CARD_NUMBER_1 = Target.the("card number field 1")
            .locatedBy("//input[@id='frmCobros:inputNuTarjetaF']");

    public static final Target CARD_NUMBER_2 = Target.the("card number field 2")
            .locatedBy("//input[contains(@id,'frmCobros:inputNuTarjetaS')]");

    public static final Target CARD_NUMBER_3 = Target.the("card number field 3")
            .locatedBy("//input[contains(@id,'frmCobros:inputNuTarjetaT')]");

    public static final Target CARD_NUMBER_4 = Target.the("card number field 4")
            .locatedBy("//input[contains(@id,'frmCobros:inputNuTarjetaL')]" +
                    "");

    public static final Target EXPIRATION_DATE = Target.the("expiration date")
            .locatedBy("//input[contains(@id,'frmCobros:inputFHVenc')]");

    public static final Target CVV = Target.the("CVV")
            .locatedBy("//input[contains(@id,'frmCobros:codSeg')]");

    public static final Target ACCEPT = Target.the("accept button")
            .locatedBy("(//span[contains(.,'Aceptar')])[1]");

    public static final Target ACCEPT_MODAL = Target.the("accept button modal")
            .locatedBy("(//span[contains(.,'Aceptar')])[5]");

    public static final Target PRODUCT_TYPE = Target.the("product type")
            .locatedBy("(//td[contains(@role,'gridcell')])[7]");

    public static final Target POLICY = Target.the("product type")
            .locatedBy("(//td[@role='gridcell'])[8]");

    public static final Target MESSAGE = Target.the("product type")
            .locatedBy("//td[@role='gridcell'][contains(.,'El número de póliza generado para el producto es:')]");

}
