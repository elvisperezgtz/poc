package mx.com.cardif.interactions;

import mx.com.cardif.user_interfaces.BaseUI;
import mx.com.cardif.utils.GetMonth;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

public class SelectDate implements Interaction {
    private String date;

    public SelectDate(String date) {
        this.date = date;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {

        String[] parts = date.split("/");
        String year = parts[0];
        String month = parts[1];
        String day = parts[2];

        while (!BaseUI.DATE_PICKER_YEAR.resolveFor(actor).getText().equals(year)) {
            actor.attemptsTo(Click.on(BaseUI.DATE_PICKER_PREVIOUS_BUTTON));
        }
        while (!BaseUI.DATE_PICKER_MONTH.resolveFor(actor).getText().equals(GetMonth.fromIntValue(month))) {
            actor.attemptsTo(Click.on(BaseUI.DATE_PICKER_PREVIOUS_BUTTON));
        }

        if (day.charAt(0) == '0') {
            day = day.replace("0", "");
        }
        actor.attemptsTo(
                Click.on(BaseUI.DATE_PICKER_DAY.of(day))
        );
    }

    public static SelectDate fromCalendar(String date) {
        return Tasks.instrumented(SelectDate.class, date);
    }
}
