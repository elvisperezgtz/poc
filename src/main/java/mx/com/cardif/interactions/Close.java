package mx.com.cardif.interactions;

import lombok.SneakyThrows;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.thucydides.core.annotations.Step;

import java.time.Duration;

import static mx.com.cardif.user_interfaces.BaseUI.CLOSE_CHANGE_PASSWORD;

public class Close implements Task {

    @SneakyThrows
    @Step("{0} closes the update password banner")
    @Override
    public <T extends Actor> void performAs(T actor) {

        if (CLOSE_CHANGE_PASSWORD.resolveFor(actor).isVisible()) {
            Thread.sleep(2000);
            actor.attemptsTo(
                    Click.on(CLOSE_CHANGE_PASSWORD.waitingForNoMoreThan(Duration.ofSeconds(10))));
        }
    }

    public static Close theUpdatePasswordBanner() {
        return Tasks.instrumented(Close.class);
    }
}
