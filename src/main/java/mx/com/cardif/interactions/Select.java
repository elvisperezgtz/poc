package mx.com.cardif.interactions;

import mx.com.cardif.user_interfaces.BaseUI;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.Step;

public class Select implements Interaction {
    private Target target;
    private String value;

    public Select(Target target, String value) {
        this.target = target;
        this.value = value;
    }

    @Override
    @Step("{0} select the option #value")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(target),
                Click.on(BaseUI.SELECT_VALUE.of(value))
        );
    }

    public static Select valueFromDropDown(Target target, String value) {
        return Tasks.instrumented(Select.class, target, value);
    }
}
