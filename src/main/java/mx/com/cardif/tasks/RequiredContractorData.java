package mx.com.cardif.tasks;

import lombok.SneakyThrows;
import mx.com.cardif.interactions.Select;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.MoveMouse;
import net.serenitybdd.screenplay.actions.SendKeys;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.Keys;

import static mx.com.cardif.user_interfaces.ContractorDataUI.*;


public class RequiredContractorData implements Task {

    public static RequiredContractorData requiredContractorData() {
        return Tasks.instrumented(RequiredContractorData.class);
    }

    @SneakyThrows
    @Step("{0} fills in the required contractor data")
    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Enter.theValue("Roberto").into(NAME),
                Enter.theValue("Valdez").into(LASTNAME),
                Enter.theValue("14/11/1989").into(BIRTHDAY),
                Enter.theValue("VAHR891114").into(RFC),
                Enter.theValue("5545138825").into(CONTACT_PHONE_NUMBER),
                Enter.theValue("correo@dominio.com").into(EMAIL),
                Enter.theValue("Calle").into(STREET),
                Enter.theValue("10").into(EXTERNAL_NUMBER),
                SendKeys.of("42111").into(POSTAL_CODE).thenHit(Keys.TAB)
        );
        Thread.sleep(2000);
        actor.attemptsTo(
                Select.valueFromDropDown(STATE, "Hidalgo"),
                MoveMouse.to(GENDER).then(Click.on(GENDER)),
                Select.valueFromDropDown(STATE, "Hidalgo"),
                MoveMouse.to(GENDER).then(Click.on(GENDER)),
                Select.valueFromDropDown(DELEGATION, "Pachuca de Soto"),
                MoveMouse.to(GENDER).then(Click.on(GENDER)),
                Select.valueFromDropDown(DELEGATION, "Pachuca de Soto"),
                MoveMouse.to(GENDER).then(Click.on(GENDER)),
                Select.valueFromDropDown(COLONY, "Santa Gertrudis"),
                MoveMouse.to(GENDER).then(Click.on(GENDER)),
                Select.valueFromDropDown(COLONY, "Santa Gertrudis"),
                MoveMouse.to(GENDER).then(Click.on(GENDER))
        );
    }
}
