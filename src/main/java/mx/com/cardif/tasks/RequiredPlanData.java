package mx.com.cardif.tasks;

import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;

import static mx.com.cardif.user_interfaces.ProductsUI.*;

public class RequiredPlanData {
    public static Performable requiredPlanData() {
        return Task.where("{0} fills in the required plan data",
                Enter.theValue("Elvis Perez").into(SALESMAN),
                Enter.theValue("cardif@prueba.com").into(SALESMAN_EMAIL),
                Enter.theValue("Principal").into(INSURANCE_COMPANY),
                Enter.theValue("3251212").into(PLAN_CONTRACT_NUMBER)
        );
    }
}
