package mx.com.cardif.tasks;

import mx.com.cardif.interactions.Select;
import mx.com.cardif.interactions.SelectDate;
import mx.com.cardif.models.Report;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.thucydides.core.annotations.Step;

import static mx.com.cardif.user_interfaces.ReportsUI.*;

public class Generate implements Task {
    private Report report;

    public Generate(Report report) {
        this.report = report;
    }

    @Override
    @Step("{0} generates a report with info")
    public <T extends Actor> void performAs(T actor) {

        actor.attemptsTo(
//                Select.valueFromDropDown(SALES_REPORT, "Reporte de Ventas"),

                Click.on(FROM_DATE),
                SelectDate.fromCalendar("2022/01/01"),
                Click.on(TO_DATE),
                SelectDate.fromCalendar("2022/01/30"),
                Select.valueFromDropDown(PARTNERS, "Scotiabank"),
                Click.on(GENERATE_XLS)
        );
    }

    public static Generate reportWithInfo(Report report) {
        return Tasks.instrumented(Generate.class, report);
    }
}
