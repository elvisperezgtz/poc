package mx.com.cardif.tasks;

import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;

import static mx.com.cardif.user_interfaces.ProductsUI.*;

public class ProductRequiredInformation {
    public static Performable productRequiredInformation() {
        return Task.where("{0} fills in the product required information",
                Enter.theValue("VW").into(BRAND),
                Enter.theValue("Nivus").into(VEHICLE_DESCRIPTION),
                Enter.theValue("2021").into(YEAR_MODEL),
                Enter.theValue("LJCPCBLCX11000237").into(SERIAL_NUMBER),
                Enter.theValue("52WVC10338").into(MOTOR_NUMBER),
                Enter.theValue("08/03/2022").into(INVOICE_DATE),
                Enter.theValue("34567890").into(CONTRACT_NUMBER)
        );
    }
}
