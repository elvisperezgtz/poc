package mx.com.cardif.tasks;

import mx.com.cardif.interactions.Select;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;

import static mx.com.cardif.user_interfaces.PaymentMethodUI.*;

public class Choose {

    public static Performable paymentMethod() {
        return Task.where("{0} choose the payment method",
                Select.valueFromDropDown(PAYMENT_METHOD, "Tarjeta Credito(VISA, MASTERCARD)"),
                Enter.theValue("ROBERTO VALDEZ").into(NAME),
                Enter.theValue("4485").into(CARD_NUMBER_1),
                Enter.theValue("5170").into(CARD_NUMBER_2),
                Enter.theValue("6055").into(CARD_NUMBER_3),
                Enter.theValue("8441").into(CARD_NUMBER_4),
                Enter.theValue("12/2027").into(EXPIRATION_DATE),
                Enter.theValue("797").into(CVV)
        );
    }
}
