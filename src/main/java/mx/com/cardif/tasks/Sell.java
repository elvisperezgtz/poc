package mx.com.cardif.tasks;

import mx.com.cardif.interactions.Select;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;

import static mx.com.cardif.user_interfaces.HomeUI.PRODUCTS;
import static mx.com.cardif.user_interfaces.ProductsUI.*;

public class Sell implements Task {

    public static Sell aGAPbyCashPayment() {
        return Tasks.instrumented(Sell.class);
    }

    @Override
    @Step("{0} sells the product: GAP by cash payment")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(PRODUCTS),
                Select.valueFromDropDown(SELECT_PRODUCT, "GAP PAGO DE CONTADO"),
                Click.on(NEXT),
                Enter.theValue("300,000.00").into(INVOICE_VALUE),
                Click.on(NEXT),
                Click.on(PLAN),
                Click.on(NEXT)
        );

    }
}
