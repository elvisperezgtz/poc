package mx.com.cardif.tasks;

import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;

import java.time.Duration;

import static mx.com.cardif.user_interfaces.PaymentMethodUI.ACCEPT;
import static mx.com.cardif.user_interfaces.PaymentMethodUI.ACCEPT_MODAL;

public class Confirm {

    public static Performable theSell() {
        return Task.where("{0} choose the payment method",
                Click.on(ACCEPT.waitingForNoMoreThan(Duration.ofSeconds(20))),
                Click.on(ACCEPT_MODAL.waitingForNoMoreThan(Duration.ofSeconds(20)))
        );
    }
}
