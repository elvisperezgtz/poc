package mx.com.cardif.models;

public class Report {
    private String salesReport;
    private String saleDateFrom;
    private String saleDateTo;
    private String partners;
    private String states;
    private String zones;
    private String branchOffices;
    private String salesPerson;
    private String products;

    public String getSalesReport() {
        return salesReport;
    }

    public String getSaleDateFrom() {
        return saleDateFrom;
    }

    public String getSaleDateTo() {
        return saleDateTo;
    }

    public String getPartners() {
        return partners;
    }

    public String getStates() {
        return states;
    }

    public String getZones() {
        return zones;
    }

    public String getBranchOffices() {
        return branchOffices;
    }

    public String getSalesPerson() {
        return salesPerson;
    }

    public String getProducts() {
        return products;
    }
}
