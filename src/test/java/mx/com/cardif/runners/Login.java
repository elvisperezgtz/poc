package mx.com.cardif.runners;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        glue = "mx.com.cardif.steps_definitions",
        features = "src/test/resources/features/posnet1/login/login.feature"
)

public class Login {
}
