package mx.com.cardif.runners;

import io.cucumber.junit.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        glue = "mx.com.cardif.steps_definitions",
        features = "src/test/resources/features/posnet1/sales/volkswagen_sales/volkswagen_sale_cash_payment.feature")
public class SalesRunner {
}
