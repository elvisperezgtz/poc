package mx.com.cardif.steps_definitions;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import mx.com.cardif.tasks.Login;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.actors.Stage;
import net.serenitybdd.screenplay.ensure.Ensure;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.util.EnvironmentVariables;
import org.openqa.selenium.WebDriver;

import static mx.com.cardif.user_interfaces.HomeUI.WELCOME_MESSAGE;

public class LoginSteps {

    @Managed(driver = "chrome")
    private WebDriver driver;
    private Actor elvis = Actor.named("Elvis");
    private EnvironmentVariables environmentVariables;

    @Before
    public void setUp() {
        Stage stage = OnStage.setTheStage(new OnlineCast());
        elvis.can(BrowseTheWeb.with(driver));

    }

    @Given("the user has access to login module")
    public void theUserHasAccessToLoginModule() {
        String page = EnvironmentSpecificConfiguration.from(environmentVariables).getProperty("login.page");
        elvis.attemptsTo(Open.url(page));
    }

    @When("he enters his credentials")
    public void heEntersHisCredentials() {
        elvis.attemptsTo(
                Login.withHisCredentials("2014042955", "Cardif123#")
        );

    }

    @Then("he will see his name in the welcome message")
    public void heWillSeeHisNameInTheWelcomeMessage() {
        elvis.attemptsTo(
                Ensure.that(WELCOME_MESSAGE.of("ROBERTO")).hasText("Bienvenid@ ROBERT")
        );

    }

}
