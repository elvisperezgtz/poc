package mx.com.cardif.steps_definitions;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import mx.com.cardif.tasks.*;
import mx.com.cardif.user_interfaces.PaymentMethodUI;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.actors.Stage;
import net.serenitybdd.screenplay.ensure.Ensure;
import net.serenitybdd.screenplay.matchers.WebElementStateMatchers;
import net.serenitybdd.screenplay.waits.WaitUntil;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.util.EnvironmentVariables;
import org.openqa.selenium.WebDriver;

import java.time.Duration;

import static java.time.Duration.ofSeconds;
import static mx.com.cardif.user_interfaces.ProductsUI.NEXT;

public class SalesSteps {
    @Managed(driver = "chrome")
    private WebDriver driver;
    private Actor elvis = Actor.named("Elvis");
    private EnvironmentVariables environmentVariables;


    @Before
    public void setUp() {
        Stage stage = OnStage.setTheStage(new OnlineCast());
        elvis.can(BrowseTheWeb.with(driver));

    }

    @Given("he is logged in on the {string} module")
    public void heIsLoggedInOnTheModule(String moduloName) {

        elvis.attemptsTo(
                Open.url(EnvironmentSpecificConfiguration.from(environmentVariables).getProperty("login.page")),
                Login.withHisCredentials("2014043018", "Cardif123#")
        );
    }

    @When("he sells a GAP by cash payment")
    public void heSellsAGAPByCashPayment() {
        elvis.attemptsTo(
                Sell.aGAPbyCashPayment()
        );
    }

    @When("he fills in the data required to sell the product")
    public void heFillsInTheDataRequiredToSellTheProduct() {
        elvis.attemptsTo(
                ProductRequiredInformation.productRequiredInformation(),
                Click.on(NEXT)
        );
    }

    @When("he fills in the plan data")
    public void heFillsInThePlanData() {
        elvis.attemptsTo(
                RequiredPlanData.requiredPlanData(),
                Click.on(NEXT)
        );
    }

    @When("he fills in the contractor's data")
    public void heFillsInTheContractorSData() {
        elvis.attemptsTo(
                RequiredContractorData.requiredContractorData(),
                Click.on(NEXT)
        );
    }

    @When("he selects a means of payment")
    public void heSelectsAMeansOfPayment() {
        elvis.attemptsTo(
                Choose.paymentMethod(),
                Click.on(NEXT));
    }

    @When("he confirms the sell")
    public void heConfirmsTheSell() {
        elvis.attemptsTo(
                Confirm.theSell()
        );
    }


    @Then("he should be able to see the product selected and the policy")
    public void heShouldBeAbleToSeeTheProductSelectedAndThePolicy() {
        elvis.attemptsTo(
                WaitUntil.the(PaymentMethodUI.MESSAGE, WebElementStateMatchers.isVisible()).forNoMoreThan(Duration.ofSeconds(15)),
                Ensure.that(PaymentMethodUI.PRODUCT_TYPE.waitingForNoMoreThan(ofSeconds(20))).hasText("GAP PAGO DE CONTADO"),
                Ensure.that(PaymentMethodUI.POLICY).text().hasSizeGreaterThan(10)
        );
    }
}
