package mx.com.cardif.steps_definitions;

import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import mx.com.cardif.models.Report;
import mx.com.cardif.tasks.Generate;
import mx.com.cardif.tasks.Login;
import mx.com.cardif.user_interfaces.HomeUI;
import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Open;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.actors.Stage;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.util.EnvironmentVariables;
import org.openqa.selenium.WebDriver;

public class GenerateReportSteps {
    @Managed(driver = "chrome")
    private WebDriver driver;
    private Actor elvis = Actor.named("Elvis");
    private EnvironmentVariables environmentVariables;

    @Before
    public void setUp() {
        Stage stage = OnStage.setTheStage(new OnlineCast());
        elvis.can(BrowseTheWeb.with(driver));

    }

    @Given("he has access to the {string} page")
    public void heHasAccessToThePage(String pageName) {
        elvis.attemptsTo(
                Open.url(EnvironmentSpecificConfiguration.from(environmentVariables).getProperty("login.page"))
                , Login.withHisCredentials("2014042955", "Cardif123#")
                , Click.on(HomeUI.OPTION_MENU.of(pageName))
        );

    }

    @When("he fills in the data required to generate the report")
    public void heFillsInTheDataRequiredToGenerateTheReport() {
        Report report = new Report();
        elvis.attemptsTo(
                Generate.reportWithInfo(report)
        );

    }

    @Then("he should be able to see the report generated")
    public void heShouldBeAbleToSeeTheReportGenerated() {

    }
}
