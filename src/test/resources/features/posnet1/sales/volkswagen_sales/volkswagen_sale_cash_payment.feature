Feature: Volkswagen sale by cash payment

  As a user
  I want a sales module
  To generate sales

  Scenario: Volkswagen sale by cash payment
    Given he is logged in on the "Sales" module
    When he sells a GAP by cash payment
    And he fills in the data required to sell the product
    And he fills in the plan data
    And he fills in the contractor's data
    And he selects a means of payment
    And he confirms the sell
    Then he should be able to see the product selected and the policy


