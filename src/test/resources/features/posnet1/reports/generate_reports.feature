Feature: Generate reports

  As an user
  I want a functionality to generate all the reports

  Scenario: Generate a sales report
    Given he has access to the "Report" page
    When he fills in the data required to generate the report
    Then he should be able to see the report generated
