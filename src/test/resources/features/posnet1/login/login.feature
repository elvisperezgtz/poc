Feature: Login

  As a user
  I want a login module
  To login with my credentials and access my profile functionalities


  Scenario: Login successful
    Given the user has access to login module
    When he enters his credentials
    Then he will see his name in the welcome message

